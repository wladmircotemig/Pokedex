//
//  SegundaViewContrller.swift
//  Pokedex
//
//  Created by COTEMIG on 21/03/22.
//

import UIKit

class SegundaViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBAction func onBackPress(_ sender: Any) {
        dismiss(animated: true)
    }
    
}
